from __future__ import unicode_literals

from django.db import models

# Create your models here.
from django.db import models


class LUser(models.Model):
    user_name = models.CharField(max_length=100, blank=False, null=False,unique=True)
    message = models.CharField(max_length=500,null=True,default='Lets start a conversation')
    latitude = models.FloatField(blank=True,null=True,max_length=200)
    longitude = models.FloatField(blank=True,null=True,max_length=200)
    orientation = models.CharField(max_length=100,null=True)
    color = models.CharField(max_length=100,null=True,default='8DD72B')

    def __unicode__(self):
        return self.user_name
