import numpy as np
from django.core.serializers import json
from django.http import HttpResponse
from django.shortcuts import render
import json
import math
from django.db import connection



from django.views.decorators.csrf import csrf_exempt

from luser.models import LUser


@csrf_exempt
def register_user(request):
    if request.method == 'POST':
        data1 = json.loads(request.body)
        json_data = data1['parameters']
        data = {}
        try:
            user_object = LUser.objects.get(user_name=json_data['user_name'])
            if user_object:
                data['error'] = 'Username already exists'
                res = {'value': 'false', 'data': data}
                return HttpResponse(json.dumps(res), content_type="application/json")

        except LUser.DoesNotExist:
            user_object = LUser()
            user_object.user_name = json_data['user_name']
            # user_object.message = json_data['message']
            # user_object.color = json_data['color']
            # user_object.orientation = json_data['orientation']
            user_object.save()
            data['user_id'] = user_object.id
            res = {'value': 'true', 'data': data}
            return HttpResponse(json.dumps(res), content_type="application/json")


@csrf_exempt
def update_user_details(request):
    if request.method == 'POST':
        json_data = json.loads(request.body)
        user_id = json_data['parameters']['user_id']
        fields = str(json_data['parameters']['fields']).split(',')
        values = str(json_data['parameters']['values']).split(',')
        data = {}
        try:
            user_object = LUser.objects.get(id=user_id)

            if 'message' in fields:
                position = int(fields.index('message'))
                message = str(values[position])
                user_object.message = message
            if 'latitude' in fields:
                position = int(fields.index('latitude'))
                latitude = values[position]
                user_object.latitude = latitude
            if 'longitude' in fields:
                position = int(fields.index('longitude'))
                longitude = (values[position])
                user_object.longitude = longitude
            if 'color' in fields:
                position = int(fields.index('color'))
                color = str(values[position])
                user_object.color = color
            if 'orientation' in fields:
                position = int(fields.index('orientation'))
                orientation = str(values[position])
                user_object.orientation = orientation

            user_object.save()

            res = {'value': 'true'}
            return HttpResponse(json.dumps(res), content_type="application/json")

        except LUser.DoesNotExist:
            data['error'] = 'Invalid User'
            res = {'value': 'false', 'data': data}
            return HttpResponse(json.dumps(res), content_type="application/json")


@csrf_exempt
def get_user_details(request):
    if request.method == 'POST':
        json_data = json.loads(request.body)
        user_id = json_data['parameters']['user_id']
        fields = str(json_data['parameters']['fields']).split(',')
        data = {}
        try:
            user_object = LUser.objects.get(id=user_id)
            result = {}
            if 'user_name' in fields:
                result['user_name'] = user_object.user_name

            if 'latitude' in fields:
                result['latitude'] = str(user_object.latitude)

            if 'longitude' in fields:
                result['longitude'] = str(user_object.longitude)

            if 'message' in fields:
                result['message'] = user_object.message

            if 'color' in fields:
                result['color'] = user_object.color

            if 'orientation' in fields:
                result['orientation'] = user_object.orientation

            res = {'value': 'true', 'data': result}
            return HttpResponse(json.dumps(res, sort_keys=True), content_type="application/json")

        except LUser.DoesNotExist:
            data['error'] = 'Invalid User'
            res = {'value': 'false', 'data': data}
            return HttpResponse(json.dumps(res), content_type="application/json")


@csrf_exempt
def get_user_locations(request):
    if request.method == 'POST':
        location_list = []
        data = {}

        users = LUser.objects.all()
        if users:
            for user in users:
                details = {}

                details['latitude'] = user.latitude
                details['longitude'] = user.longitude

                details['id'] = user.id
                location_list.append(details)

            res = {'value': 'true', 'data': location_list}
            return HttpResponse(json.dumps(res), content_type="application/json")
        else:
            data['error'] = 'Invalid User'
            res = {'value': 'false', 'data': data}
            return HttpResponse(json.dumps(res), content_type="application/json")

    else:
        res = {'value': 'false'}
        return HttpResponse(json.dumps(res), content_type="application/json")


@csrf_exempt
def get_nearby_users(request):
    if request.method == 'POST':
        data1 = json.loads(request.body)
        json_data = data1['parameters']

        location_list1 = []
        data={}

        # lat1 =(42.336897)  # Current dd lat point converted to radians
        # lng1 =(-71.145786)
        lat1 = json_data['latitude']
        lng1 = json_data['longitude']
        try:
            users = LUser.objects.filter(latitude__isnull=False,longitude__isnull=False)
            # users = LUser.objects.all()
            for user in users:
                details = {}
                lat2 = user.latitude
                lng2 = user.longitude

                distance = get_distance(lat1, lng1, lat2, lng2)
                print distance

                if distance <= 0.2:
                    details['latitude'] = str(lat2)
                    details['longitude'] = str(lng2)
                    details['id'] = user.id
                    details['name'] = user.user_name
                    details['color'] = user.color
                    location_list1.append(details)

            res = {'value': 'true', 'data': location_list1}
            return HttpResponse(json.dumps(res), content_type="application/json")

        except LUser.DoesNotExist:
            data['error'] = 'Invalid User'
            res = {'value': 'false','data':data}
            return HttpResponse(json.dumps(res), content_type="application/json")


@csrf_exempt
def get_distance(lat1, lng1, lat2, lng2):

    R = 6371 # radius of earth in m
    lat1rads = math.radians(lat1)
    lat2rads = math.radians(lat2)
    deltaLat = math.radians((lat2 - lat1))
    deltaLng = math.radians((lng2 - lng1))
    a = math.sin(deltaLat / 2) * math.sin(deltaLat / 2) + math.cos(lat1rads) * math.cos(lat2rads) * math.sin(
        deltaLng / 2) * math.sin(deltaLng / 2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    d = R * c

    return d


# @csrf_exempt
# def getDestinationLatLong(lat=28.455556, lng=-80.527778, azimuth=360, distance=200):
#     '''returns the lat an long of destination point
#     given the start lat, long, aziuth, and distance'''
#     R = 6378.1  # Radius of the Earth in km
#     brng = math.radians(azimuth)  # Bearing is degrees converted to radians.
#     d =float( distance )/ 1000  # Distance m converted to km
#     lat1 = math.radians(28.455556)  # Current dd lat point converted to radians
#     lon1 = math.radians(-80.527778)  # Current dd long point converted to radians
#     lat2 = math.asin(math.sin(lat1) * math.cos(d / R) + math.cos(lat1) * math.sin(d / R) * math.cos(brng))
#     lon2 = lon1 + math.atan2(math.sin(brng) * math.sin(d / R) * math.cos(lat1),
#                              math.cos(d / R) - math.sin(lat1) * math.sin(lat2))
#     # convert back to degrees
#     lat2 = math.degrees(lat2)
#     lon2 = math.degrees(lon2)
#     return [lat2, lon2]
#
#
# @csrf_exempt
# def getlocations(request):
#     if request.method == 'POST':
#         data1 = json.loads(request.body)
#         json_data = data1['parameters']
#         location_list = []
#         location_list1 = []
#         data = {}
#         details = {}
#         x=getDestinationLatLong(json_data['latitude'],json_data['latitude'],azimuth=360, distance=200)
#         # latitude=np.arange(28.455556, 28.457352,0.000001)
#         latitude=np.arange(json_data['latitude'], 28.457352,0.000001)
#         latitude_list = []
#         latitude_list.append(latitude)
#
#         # longitude = np.arange(-80.527785, -80.527779, 0.000001)
#         longitude = np.arange(json_data['latitude'], -80.527779, 0.000001)
#         longitude_list = []
#         longitude_list.append(longitude)
#         try:
#             users = LUser.objects.filter(latitude__in=latitude_list[0],longitude__in=longitude_list[0])
#             for user in users:
#                 details['latitude']=user.latitude
#                 details['longitude']=user.longitude
#
#                 location_list1.append(details)
#             res = {'value': 'true', 'data': location_list1}
#             return HttpResponse(json.dumps(res), content_type="application/json")
#         except LUser.DoesNotExist:
#             data['error'] = 'Invalid User'
#             res = {'value': 'false', 'data': data}
#             return HttpResponse(json.dumps(res), content_type="application/json")
#
#
#     else:
#         res = {'value': 'false'}
#         return HttpResponse(json.dumps(res), content_type="application/json")
#
#
# @csrf_exempt
# def test(request):
#     if request.method == 'POST':
#         details={}
#         location_list1=[]
#         lat_list=[]
#         lng_list=[]
#         distance=float(0.2)
#         radius=float(6371)
#         lat =(28.455556)  # Current dd lat point converted to radians
#         lng =(-80.527778)
#         maxlat = lat+ (distance / radius)
#         print maxlat
#         latitude=np.arange(lat,maxlat,0.000001)
#         lat_list.append(latitude)
#         print lat_list
#
#         maxlng = lng +(distance / radius / math.cos((lat)))
#         print maxlng
#         longitude = np.arange(lng,maxlng,-0.000001)
#         lng_list.append(longitude)
#         try:
#
#             users = LUser.objects.filter(latitude__in=lat_list[0],longitude__in=lng_list[0])
#             for user in users:
#                 details['latitude']=user.latitude
#                 details['longitude']=user.longitude
#
#                 location_list1.append(details)
#             res = {'value': 'true', 'data': location_list1}
#             return HttpResponse(json.dumps(res), content_type="application/json")
#         except Exception as Err:
#             res = {'value':Err}
#             return HttpResponse(json.dumps(res), content_type="application/json")
#
#     else:
#         res = {'value': 'false'}
#         return HttpResponse(json.dumps(res), content_type="application/json")

