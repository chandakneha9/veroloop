"""location URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from luser.views import *

urlpatterns = [
    url(r'^admin/', admin.site.urls),
url(r'^register_user$', register_user, name='register_user'),
url(r'^update_user_details$', update_user_details, name='update_user_details'),
url(r'^get_user_details$', get_user_details, name='get_user_details'),
url(r'^get_user_locations$', get_user_locations, name='get_user_locations'),
url(r'^get_nearby_users$', get_nearby_users, name='get_nearby_users'),
url(r'^get_distance$', get_distance, name='get_distance'),
]
