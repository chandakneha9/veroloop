"""
WSGI config for location project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/howto/deployment/wsgi/
"""

import os
import sys

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "location.settings")
sys.path.append('/home/clisorigin/webapps/veraloop/veroloop')  # Path to django project where manage.py exists
sys.path.append('/home/clisorigin/webapps/veraloop')  # Path to django directory

application = get_wsgi_application()
